import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShortenPipe } from './shorten.pipe';



@NgModule({
  declarations: [
    ShortenPipe
  ],
  exports: [
    ShortenPipe
  ],
  imports: [
    CommonModule
  ]
})
export class ShortenModule { }
