import { Pipe, PipeTransform } from '@angular/core';
const MAX_LENGTH = 12;
@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    let ret = '';
    if (value.length > MAX_LENGTH) {
      ret = value.substring(0, 7);
      ret += '...';
      ret += value.substring(value.length - 6, value.length);
    }
    else {
      ret = value;
    }

    return ret;
  }

}
