import {Account} from './account.model';

export interface EvenOddGame {even: boolean; betWeiAmount: number; account: Account; contractAddress: string;}

export interface NumberGame {num: number; betWeiAmount: number; account: Account; contractAddress: string;}
