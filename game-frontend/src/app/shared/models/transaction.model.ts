export interface Transaction {
  block_hash: string;
  block_number: number;
  from_add: string;
  gas: number;
  mined_on: string;
  to_add: string;
  tx_hash: string;
  value: string;
}
