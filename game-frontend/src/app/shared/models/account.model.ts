export interface Account {
  balance: number;
  address: string;
}
