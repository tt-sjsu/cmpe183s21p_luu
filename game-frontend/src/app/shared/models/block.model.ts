export interface Block {
  block_number: number;
  mined_on: string;
  gas: number;
}
