export interface Contract {
  contract_address: string;
  owner_address: string;
  active: boolean;
  bet_amount: number;
  hint: number;
}
