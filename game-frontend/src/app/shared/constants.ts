export const ETHER_TO_WEI = 1000000000000000000;


export function ether_to_wei(ether: number): string {
  return (ether / ETHER_TO_WEI).toFixed(4);
}

export function wei_to_ether(wei: number): string {
  return (wei / ETHER_TO_WEI).toFixed(4);
}
