import { Component, OnInit } from '@angular/core';
import {Block} from '../shared/models/block.model';
import {Subscription} from 'rxjs';
import {DataService} from '../data.service';
import {Transaction} from '../shared/models/transaction.model';
import {wei_to_ether} from '../shared/constants';

@Component({
  selector: 'app-transactions',
  template: `
    <div class="transactions" fxLayout="column" fxLayoutGap="2rem">
      <h3>Transaction Information</h3>
      <table mat-table [dataSource]="transactions" class="mat-elevation-z8">

        <!--- Note that these columns can be defined in any order.
              The actual rendered columns are set as a property on the row definition" -->

        <ng-container matColumnDef="block_number">
          <th mat-header-cell *matHeaderCellDef> Block Number </th>
          <td mat-cell *matCellDef="let element"> {{element.block_number}} </td>
        </ng-container>

        <ng-container matColumnDef="mined_on">
          <th mat-header-cell *matHeaderCellDef> Mined on </th>
          <td mat-cell *matCellDef="let element"> {{element.mined_on}} </td>
        </ng-container>

        <ng-container matColumnDef="gas">
          <th mat-header-cell *matHeaderCellDef> Gas </th>
          <td mat-cell *matCellDef="let element"> {{element.gas}} </td>
        </ng-container>
        <ng-container matColumnDef="value">
          <th mat-header-cell *matHeaderCellDef> Value (Ether) </th>
          <td mat-cell *matCellDef="let element"> {{wei_to_ether(element.value)}} </td>
        </ng-container>

        <ng-container matColumnDef="from_add">
          <th mat-header-cell *matHeaderCellDef> From Address </th>
          <td mat-cell *matCellDef="let element" [matTooltip]="element.from_add"> {{element.from_add | shorten}} </td>
        </ng-container>

        <ng-container matColumnDef="tx_hash">
          <th mat-header-cell *matHeaderCellDef> Transaction Hash </th>
          <td mat-cell *matCellDef="let element" [matTooltip]="element.tx_hash"> {{element.tx_hash | shorten}} </td>
        </ng-container>

        <ng-container matColumnDef="block_hash">
          <th mat-header-cell *matHeaderCellDef> Block Hash </th>
          <td mat-cell *matCellDef="let element" [matTooltip]="element.block_hash"> {{element.block_hash | shorten}} </td>
        </ng-container>

        <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
        <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
      </table>
    </div>
  `,
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  public wei_to_ether = wei_to_ether;
  public transactions: Transaction[] = [];
  displayedColumns: string[] = ['block_number', 'mined_on', 'gas', 'value', 'from_add', 'tx_hash', 'block_hash'];
  private subscription = new Subscription();
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.getAllTransactions();
    this.subscription.add(
      this.dataService.transactions$.subscribe(transactions => this.transactions = transactions)
    );
  }

}
