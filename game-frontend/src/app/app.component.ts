import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-header></app-header>
<!--    <app-home [contracts]="contracts$ | async"-->
<!--        [account]="currentAccount$ | async"-->
<!--        [blocks]="blocks$ | async"-->
<!--        (betEvenOddGame)="playEvenOddGame($event)"-->
<!--        (betNumberGame)="playNumberGame($event)"-->
<!--    ></app-home>-->
    <router-outlet></router-outlet>
    <footer style="text-align: center">
      Copyright ©2021 THT CMPE-183
    </footer>
  `,
})
export class AppComponent {
  title = 'game-frontend';
}
