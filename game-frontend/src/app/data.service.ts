import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Account} from './shared/models/account.model';
import {Contract} from './shared/models/contract.model';
import {Block} from './shared/models/block.model';
import {environment} from '../environments/environment';
import {map, shareReplay} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {EvenOddGame, NumberGame} from './shared/models/game.model';
import {Router} from '@angular/router';
import {Transaction} from './shared/models/transaction.model';

@Injectable({
  providedIn: 'root',
})
export class DataService {

  public accounts$ = new BehaviorSubject<Account[]>(undefined);
  public contracts$ = new BehaviorSubject<Contract[]>(undefined);
  public blocks$ = new BehaviorSubject<Block[]>(undefined);
  public currentAccount$ = new BehaviorSubject<Account>(undefined);
  public transactions$ = new BehaviorSubject<Transaction[]>(undefined);
  constructor(private http: HttpClient, private router: Router) {}


  public updatebalance(address: string): void {
    this.http.get<Account>(environment.API_URL + '/account/' + address).pipe(
      map((a) => this.currentAccount$.next(a))
    ).subscribe();
  }

  public getAllContracts(): void {
    this.http.get<{ contracts: Contract[] }>(environment.API_URL + '/contracts').pipe(
      map(({contracts}) => this.contracts$.next(contracts))
    ).subscribe();
  }

  public getAllBlocks(): void {
    this.http.get<{ blocks: Block[] }>(environment.API_URL + '/blocks').pipe(
      map(({blocks}) => this.blocks$.next(blocks))
    ).subscribe();
  }
  public getAllAccounts(): void {
    this.http.get<{accounts: Account[] }>(environment.API_URL + '/accounts').pipe(
      map(({accounts}) => this.accounts$.next(accounts))
    ).subscribe();
  }
  public getAllTransactions(): void {
    this.http.get<{transactions: Transaction[] }>(environment.API_URL + '/transactions').pipe(
      map(({transactions}) => this.transactions$.next(transactions))
    ).subscribe();
  }

  public playEvenOddGame(evenOddGame: EvenOddGame): void {
    this.http.post(`${environment.API_URL}/evenoddgame/${evenOddGame.account.address}/contract/${evenOddGame.contractAddress}/${evenOddGame.betWeiAmount}/${evenOddGame.even}`, null).subscribe(
      () => {
        this.updatebalance(evenOddGame.account.address);
        this.getAllContracts();
      }
    );
  }

  public playNumberGame(numberGame: NumberGame): void {
    this.http.post(`${environment.API_URL}/numbergame/${numberGame.account.address}/contract/${numberGame.contractAddress}/${numberGame.betWeiAmount}/${numberGame.num}`, null).subscribe(
      () => {
        this.updatebalance(numberGame.account.address);
        this.getAllContracts();
      }
    );
  }

}
