import { Component, Input, Output, EventEmitter } from '@angular/core';
import {Account} from '../shared/models/account.model';
import {ether_to_wei} from '../shared/constants';
import {HttpClient} from '@angular/common/http';
import {DataService} from '../data.service';

@Component({
  selector: 'app-header',
  template: `
    <p class="header">
      <mat-toolbar>
        <a mat-icon-button routerLink="/home">
            <img src="assets/images/SJSU.jpg">
        </a>
        <span>Game THT - CMPE 183</span>
        <a class="link" routerLink="/home">Home</a>
        <a class="link" routerLink="/blocks">Blocks</a>
        <a class="link" routerLink="/transactions">Transactions</a>
        <span class="example-spacer"></span>
        <ng-container *ngIf="dataService.accounts$ | async as accounts">
          <ng-container *ngIf="dataService.currentAccount$ | async as account">
            <span *ngIf="account">{{ether_to_wei(account.balance)}}  Ether </span>
          </ng-container>
          <mat-form-field appearance="fill" color="primary">
            <mat-label>Account</mat-label>
            <mat-select (valueChange)="selectChange($event)">
              <mat-option *ngFor="let account of accounts; let i = index" [value]="account.address">
                [{{i}}] {{account.address}}
              </mat-option>
            </mat-select>
          </mat-form-field>
        </ng-container>
      </mat-toolbar>
    </p>
  `,
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Output() selectAccount = new EventEmitter<string>();
  ether_to_wei = ether_to_wei;

  constructor(private http: HttpClient, public dataService: DataService) {
    this.dataService.getAllAccounts();
  }

  public selectChange(address: string): void {
    this.dataService.updatebalance(address);
  }

}
