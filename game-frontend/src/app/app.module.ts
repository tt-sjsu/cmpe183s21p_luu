import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {HomeComponent} from './home/home.component';
import {HeaderModule} from './header/header.module';
import {HttpClientModule} from '@angular/common/http';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';
import {DataService} from './data.service';
import {BlocksComponent} from './blocks/blocks.component';
import {TransactionsComponent} from './transactions/transactions.component';
import {ShortenModule} from './shared/pipes/shorten/shorten.module';

@NgModule({
  declarations: [
    AppComponent, HomeComponent, BlocksComponent, TransactionsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule,
    FlexLayoutModule,
    AppRoutingModule,
    HttpClientModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    FormsModule,
    HeaderModule,
    ShortenModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
