import {Component, Input, Output, OnChanges, SimpleChanges, EventEmitter, OnInit} from '@angular/core';
import {Contract} from '../shared/models/contract.model';
import {ether_to_wei, ETHER_TO_WEI} from '../shared/constants';
import {Account} from '../shared/models/account.model';
import {EvenOddGame, NumberGame} from '../shared/models/game.model';
import {Block} from '../shared/models/block.model';
import {environment} from '../../environments/environment';
import {map, shareReplay} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Subscription} from 'rxjs';
import {DataService} from '../data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  template: `
    <div class="home" fxLayout="column" fxLayoutGap="2rem">
      <h3>Games</h3>
      <ng-container *ngFor="let contract of contracts; let i = index">
        <div *ngIf="contract.active" class="contract-divider">
          <h2 fxLayoutAlign="space-between center">
            <span>Game {{i}}</span>
            <mat-icon [matTooltip]="contract.hint">help_outline</mat-icon>
            <span>{{ether_to_wei(contract.bet_amount)}} Ether</span>
          </h2>

          <div class="game">
            <h4 fxLayoutAlign="space-between center">
              <span>Bet even/odd (odd ratio: 1 to 1)</span>
              <span>Max bet: {{ether_to_wei(contract.bet_amount)}} Ether</span>
            </h4>
            <div fxLayoutAlign="start center" fxLayoutGap="2rem">
              <mat-form-field class="example-form-field" appearance="fill">
                <mat-label>Bet amount in Ether</mat-label>
                <input matInput type="number" min="0.0001" [max]="contract.bet_amount / ETHER_TO_WEI" required
                       [(ngModel)]="games[i].evenOdd">
              </mat-form-field>
              <button mat-raised-button class="even"
                      [disabled]="!checkEvenOddValid(games[i].evenOdd, 0.0001, contract.bet_amount / ETHER_TO_WEI, contract.owner_address)"
                      (click)="betEvenOdd(true, games[i].evenOdd, account, contract.contract_address)"
              >Even
              </button>
              <button mat-raised-button class="odd"
                      [disabled]="!checkEvenOddValid(games[i].evenOdd, 0.0001, contract.bet_amount / ETHER_TO_WEI, contract.owner_address)"
                      (click)="betEvenOdd(false, games[i].evenOdd, account, contract.contract_address)"
              >Odd
              </button>
            </div>
            <mat-error *ngIf="!checkEvenOddValid(games[i].evenOdd, 0.0001, contract.bet_amount / ETHER_TO_WEI, contract.owner_address)">
              Invalid Amount or Address
            </mat-error>
          </div>

          <div class="game">
            <h4 fxLayoutAlign="space-between center">
              <span>Bet Number from 1 to 6 (odd ratio: 1 to 2)</span>
              <span>Max bet: {{ether_to_wei(contract.bet_amount / 2)}} ether</span>
            </h4>
            <div fxLayoutAlign="start center" fxLayoutGap="2rem">
              <mat-form-field class="example-form-field" appearance="fill">
                <mat-label>Bet amount in Ether</mat-label>
                <input matInput type="number" min="0.0001" [max]="contract.bet_amount / ETHER_TO_WEI / 2" required
                       [(ngModel)]="games[i].number">
              </mat-form-field>
              <ng-container *ngFor="let n of [1,2,3,4,5,6]">

                <button mat-raised-button class="number"
                        [disabled]="!checkEvenOddValid(games[i].number, 0.0001, contract.bet_amount / ETHER_TO_WEI / 2, contract.owner_address)"
                        (click)="betNumber(n, games[i].evenOdd, account, contract.contract_address)"
                >{{n}}</button>
              </ng-container>
            </div>
            <mat-error *ngIf="!checkEvenOddValid(games[i].number, 0.0001, contract.bet_amount / ETHER_TO_WEI / 2, contract.owner_address)">
              Invalid Amount or Address
            </mat-error>
          </div>
        </div>

      </ng-container>
    </div>
  `,
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public ETHER_TO_WEI = ETHER_TO_WEI;
  public contracts: Contract[] = [];
  public account: Account;

  ether_to_wei = ether_to_wei;
  public games: {evenOdd: number; number: number}[] = [];
  private subscription = new Subscription();
  constructor(private dataService: DataService, private router: Router) {}

  ngOnInit(): void {
      this.subscription.add(
        this.dataService.contracts$.subscribe(
          (contracts: Contract[]) => {
            this.contracts = contracts;
            if (this.contracts) {
              this.games = this.contracts.map((contract: Contract) => (
                {
                  evenOdd: 1,
                  number: 1,
                })
              );
            }
          }
        )
      );

      this.subscription.add(
        this.dataService.currentAccount$.subscribe(
          (account: Account) => {
            this.account = account;
          }
        )
      );
      this.dataService.getAllContracts();
  }


  public checkEvenOddValid(value, min, max, contractOwnerAddress): boolean {
    return this.account && this.account.balance && value <= max && value >= min && this.account.address !== contractOwnerAddress;
  }

  public betEvenOdd(even: boolean, ether: number, account: Account, contractAddress: string): void {
    this.dataService.playEvenOddGame({even, betWeiAmount: ether * ETHER_TO_WEI, account, contractAddress});
  }

  public betNumber(num: number, ether: number, account: Account, contractAddress: string): void {
    this.dataService.playNumberGame({num, betWeiAmount: ether * ETHER_TO_WEI, account, contractAddress});
  }


}
