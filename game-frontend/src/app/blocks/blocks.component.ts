import {Component, Input, OnInit} from '@angular/core';
import {Block} from '../shared/models/block.model';
import {Observable, Subscription} from 'rxjs';
import {DataService} from '../data.service';

@Component({
  selector: 'app-blocks',
  template: `
    <div class="blocks" fxLayout="column" fxLayoutGap="2rem">
      <h3>Blocks Information</h3>
      <table mat-table [dataSource]="blocks" class="mat-elevation-z8">

        <!--- Note that these columns can be defined in any order.
              The actual rendered columns are set as a property on the row definition" -->

        <ng-container matColumnDef="block_number">
          <th mat-header-cell *matHeaderCellDef> Block Number </th>
          <td mat-cell *matCellDef="let element"> {{element.block_number}} </td>
        </ng-container>

        <ng-container matColumnDef="mined_on">
          <th mat-header-cell *matHeaderCellDef> Mined on </th>
          <td mat-cell *matCellDef="let element"> {{element.mined_on}} </td>
        </ng-container>

        <ng-container matColumnDef="gas">
          <th mat-header-cell *matHeaderCellDef> Gas </th>
          <td mat-cell *matCellDef="let element"> {{element.gas}} </td>
        </ng-container>

        <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
        <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
      </table>
    </div>

  `,
  styleUrls: ['./blocks.component.scss']
})
export class BlocksComponent implements OnInit {

  public blocks: Block[] = [];
  displayedColumns: string[] = ['block_number', 'mined_on', 'gas'];
  private subscription = new Subscription();
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.getAllBlocks();
    this.subscription.add(
      this.dataService.blocks$.subscribe(blocks => this.blocks = blocks)
    );
  }

}
