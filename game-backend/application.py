from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
from web3 import Web3
import json
import os

ETHER_TO_WEI = 1000000000000000000

rpc_serer = "http://127.0.0.1:7545" #ethereum network

compiled_contract_path = 'build/contracts/Game.json'

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
db = SQLAlchemy(app)

migrate = Migrate(app, db)

class Contract(db.Model):
    contract = db.Column(db.String(80), primary_key=True)
    owner_address = db.Column(db.String(120))
    active = db.Column(db.Boolean(), default=True)

def object_as_dict(contract: Contract):
    return {
        'contract_address': contract.contract,
        'owner_address': contract.owner_address,
        'active': contract.active
    }

@app.route('/')
def index():
    return 'Hello!'

@app.route('/contracts', methods = ['POST', 'GET'])
def createcontract():
    web3 = Web3(Web3.HTTPProvider(rpc_serer))
    if request.method == 'POST':
        # Set the default account (so we don't need to set the "from" for every transaction call)
        web3.eth.defaultAccount = web3.eth.accounts[0]

        contract_address = deploy_contract(web3, compiled_contract_path)
        contract = Contract(contract=contract_address, owner_address=web3.eth.defaultAccount, active=True)
        db.session.add(contract)
        db.session.commit()

    with open(compiled_contract_path) as file:
        contract_json = json.load(file)  # load contract info as JSON
        contract_abi = contract_json['abi']  # fetch contract's abi - necessary to call its functions

    # contracts = Contract.query.filter(Contract.active==True).all()
    contracts = Contract.query.all()

    contracts_json = [object_as_dict(row) for row in contracts]
    for i in range(len(contracts_json)):
        game = web3.eth.contract(
            address=contracts_json[i]['contract_address'],
            abi=contract_abi
        )
        contract_code = web3.eth.getCode(contracts_json[i]['contract_address'])
        if not contract_code: #Contract already get destroyed
            Contract.query.filter(Contract.contract == contracts_json[i]['contract_address']).update({Contract.active: False})
            db.session.commit()
            contracts_json[i]['active'] = False
        else:
            bet_amount = game.functions.getBalance().call()
            contracts_json[i]['bet_amount'] = bet_amount
            hint = game.functions.getData().call()
            contracts_json[i]['hint'] = hint

    return {"contracts": contracts_json}

@app.route('/accounts', methods = ['GET'])
def accounts():
    web3 = Web3(Web3.HTTPProvider(rpc_serer))
    return {'accounts': [dict(address=addr, balance=web3.eth.getBalance(addr)) for addr in web3.eth.accounts]}


@app.route('/account/<address>', methods = ['GET'])
def account(address):
    web3 = Web3(Web3.HTTPProvider(rpc_serer))
    return dict(address=address, balance=web3.eth.getBalance(address))

@app.route('/evenoddgame/<address>/contract/<contract_address>/<wei>/<even>', methods = ['POST'])
def playEvenOdd(address, contract_address, wei, even):
    web3 = Web3(Web3.HTTPProvider(rpc_serer))

    web3.eth.defaultAccount = address

    with open(compiled_contract_path) as file:
        contract_json = json.load(file)  # load contract info as JSON
        contract_abi = contract_json['abi']  # fetch contract's abi - necessary to call its functions
    game = web3.eth.contract(
        address=contract_address,
        abi=contract_abi
    )
    tx_hash = game.functions.betEvenOdd(even == 'true').transact({
        'value': int(wei)
    })
    web3.eth.wait_for_transaction_receipt(tx_hash)
    Contract.query.filter(Contract.contract==contract_address).update({Contract.active: False})
    db.session.commit()
    return dict()

@app.route('/numbergame/<address>/contract/<contract_address>/<wei>/<number>', methods = ['POST'])
def playNumber(address, contract_address, wei, number):
    web3 = Web3(Web3.HTTPProvider(rpc_serer))

    web3.eth.defaultAccount = address

    with open(compiled_contract_path) as file:
        contract_json = json.load(file)  # load contract info as JSON
        contract_abi = contract_json['abi']  # fetch contract's abi - necessary to call its functions
    game = web3.eth.contract(
        address=contract_address,
        abi=contract_abi
    )

    tx_hash = game.functions.betNumber(int(number)).transact({
        'value': int(wei)
    })
    web3.eth.wait_for_transaction_receipt(tx_hash)
    Contract.query.filter(Contract.contract==contract_address).update({Contract.active: False})
    db.session.commit()
    return dict()

from datetime import datetime
@app.route('/blocks')
def blocks_view():
    web3 = Web3(Web3.HTTPProvider(rpc_serer))
    last_block = web3.eth.blockNumber
    blocks = []
    while last_block >= 0 :
        bl_info = web3.eth.getBlock(last_block)
        timestampe = bl_info['timestamp']
        dt_obj = datetime.fromtimestamp(timestampe)
        block_info = {'block_number': last_block, 'gas': bl_info['gasUsed'], 'mined_on': dt_obj }
        last_block = last_block -1
        blocks.append(block_info)
    return {"blocks": blocks}

@app.route('/blocks/<id>', methods = ['GET'])
def block_info(id):
    web3 = Web3(Web3.HTTPProvider(rpc_serer))
    block_id = int(id)
    bl_info = web3.eth.getBlock(block_id)
    if block_id == 0:
        return {}
    tx_info = web3.eth.get_transaction_by_block(block_id, 0)
    # tx_info = web3.eth.getTransactionFromBlock(block_id, 0)
    timestampe = bl_info['timestamp']
    dt_obj = datetime.fromtimestamp(timestampe)

    block_info = {"block_number": tx_info['blockNumber'],
                  "gas": bl_info['gasUsed'],
                  "mined_on": dt_obj,
                  "block_hash": web3.toHex(bl_info['hash']),
                  "tx_hash":  web3.toHex(bl_info['transactions'][0]),
                  "from_add":  (tx_info['from']),
                  "to_add":  (tx_info['to']),
                  "value": tx_info['value']
                  }
    return   block_info

@app.route('/transactions', methods = ['GET'])
def transactions():
    web3 = Web3(Web3.HTTPProvider(rpc_serer))
    last_block = web3.eth.blockNumber
    transactions = []
    while last_block >= 1:
        transactions.append(block_info(last_block))
        last_block = last_block -1
    return {'transactions': transactions}

def deploy_contract(web3, compiled_contract_path):
    # DEPLOY CONTRACT
    with open(compiled_contract_path) as file:
        contract_json = json.load(file)  # load contract info as JSON
        contract_abi = contract_json['abi']  # fetch contract's abi - necessary to call its functions
        bytecode = contract_json['bytecode']

    Game = web3.eth.contract(abi=contract_abi, bytecode=bytecode)
    # Submit the transaction that deploys the contract
    tx_hash = Game.constructor().transact({
        'value': 2 * ETHER_TO_WEI
    })
    tx_receipt = web3.eth.wait_for_transaction_receipt(tx_hash)
    return tx_receipt['contractAddress']

if __name__ == "__main__":
    app.run(host="0.0.0.0")