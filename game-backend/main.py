from web3 import Web3
import json

ETHER_TO_WEI = 1000000000000000000

def deploy_contract(compiled_contract_path):
    # DEPLOY CONTRACT
    with open(compiled_contract_path) as file:
        contract_json = json.load(file)  # load contract info as JSON
        contract_abi = contract_json['abi']  # fetch contract's abi - necessary to call its functions
        bytecode = contract_json['bytecode']

    Game = web3.eth.contract(abi=contract_abi, bytecode=bytecode)
    # Submit the transaction that deploys the contract
    tx_hash = Game.constructor().transact({
        'value': 2 * ETHER_TO_WEI
    })
    tx_receipt = web3.eth.wait_for_transaction_receipt(tx_hash)
    game = web3.eth.contract(
        address=tx_receipt.contractAddress,
        abi=contract_abi
    )
    return tx_receipt['contractAddress']

if __name__ == "__main__":
    rpc_serer = "http://127.0.0.1:7545"
    web3 = Web3(Web3.HTTPProvider(rpc_serer))

    print(web3.isConnected())

    # Set the default account (so we don't need to set the "from" for every transaction call)
    web3.eth.defaultAccount = web3.eth.accounts[0]


    print(web3.eth.block_number)
    balance = web3.eth.getBalance(web3.eth.accounts[0])

    print(web3.fromWei(balance, 'ether'))

    # Path to the compiled contract JSON file
    compiled_contract_path = 'build/contracts/Game.json'
    print(deploy_contract(compiled_contract_path))


    # # Fetch deployed contract reference
    # contract = web3.eth.contract(address=deployed_contract_address, abi=contract_abi)
    #
    # # Call contract function (this is not persisted to the blockchain)
    # message = contract.functions.sayHello().call()
    #
    # print(message)