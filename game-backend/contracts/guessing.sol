pragma solidity ^0.6.6;

contract Game {

    uint originalBalance;
    uint8 data;
    address payable public admin;
    
    constructor() public payable {
          data = uint8(uint256(keccak256(abi.encodePacked(block.timestamp, block.difficulty))) % 6 + 1); // random from 1->6
          admin = msg.sender;
          originalBalance = msg.value;
    }

    modifier onlyAdmin() {
      require(msg.sender == admin, 'caller is not the admin');
      _;
    }
  
    function getData() public view returns(uint) {
        return data;
    }
    
    function getBalance() public view returns(uint) {
        return address(this).balance;
    }

    
    function betNumber(uint8 betValue) public payable {
        require(msg.value <= originalBalance / 2, "too much money");
        require(betValue <= 6 && betValue > 0, "bet out of range");
        uint total_eth_in_game = msg.value * 3; 
        if (betValue == data) {
            msg.sender.transfer(total_eth_in_game);
        }
        
        selfdestruct(admin);
    }

    
    function betEvenOdd(bool even) public payable {
        require(msg.value <= originalBalance, "too much money");
        uint total_eth_in_game = msg.value * 2; 
        if (even == (data % 2 == 0)) {
            msg.sender.transfer(total_eth_in_game);
        }
        
        selfdestruct(admin);
    }

   function withdrawEther() external payable onlyAdmin {
       uint balance = address(this).balance;
       admin.transfer(balance);

   }
  
}