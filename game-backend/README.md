## haha

https://www.codementor.io/@gcrsaldanha/part-1-2-deploy-a-smart-contract-on-ethereum-with-python-truffle-and-web3py-1buhsxibhf


## Step to build and run program
0. Install [Ganache](https://www.trufflesuite.com/ganache)

1. Follow [tutorial](https://www.codementor.io/@gcrsaldanha/part-1-2-deploy-a-smart-contract-on-ethereum-with-python-truffle-and-web3py-1buhsxibhf)

```bash
npm install truffle
./node_modules/.bin/truffle init

./node_modules/.bin/truffle compile
./node_modules/.bin/truffle migrate
./node_modules/.bin/truffle develop

```
### RUN Flask app
1) set up database
```bash
export FLASK_APP=application.py
export FLASK_ENV=development
flask db init
```

## Upgrade database
```bash
export FLASK_APP=application.py
export FLASK_ENV=development
flask db migrate -m "Initial migration."
flask db upgrade
```

### Create virtualenv (Mac OS)
```bash
virtualenv -p python3 .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```
### Run
```bash
python application.py
```

### Create more game
```bash
for i in {1..10}; do curl -X POST localhost:5000/contracts; done
```